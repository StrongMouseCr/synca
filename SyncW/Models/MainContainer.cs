﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using SyncW.Hub;
using Timer = System.Timers.Timer;

namespace SyncW.Models
{
    public class MainContainer : IHostedService
    {
        private Timer _mainTimer;
        private IHubContext<SyncHub, ISyncHub> _hub;

        public MainContainer(IHubContext<SyncHub, ISyncHub> hub)
        {
            _hub = hub;
            SetTimer();
        }
        
        private void SetTimer()
        {
            _mainTimer = new Timer(1000);
            _mainTimer.Elapsed += OnTimedEvent;
            _mainTimer.AutoReset = true;
            _mainTimer.Enabled = true;
        }
        
        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            using var memoryImage = new Bitmap(1920, 1080);
            ScreenCapture.TakeScreenShot(memoryImage);
            var imgByteArray = ScreenCapture.ImageToBytes(memoryImage);
            var packet = ImagePacket.GetRawData(imgByteArray);
            _hub.Clients.All.ReceiveImage(packet);
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}