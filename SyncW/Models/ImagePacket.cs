﻿namespace SyncW.Models
{
    public static class ImagePacket
    {
        public static byte[] GetRawData(byte[] imgSources) =>
            ScreenCapture.DecodeBytes(ScreenCapture.EncodeBytes(imgSources));
    }
}