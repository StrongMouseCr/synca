﻿using System;
using System.Drawing;

namespace SyncW.Models
{
    public static class ScreenCapture
    {
        public static void TakeScreenShot(Bitmap memoryImage)
        {
            var size = new Size(memoryImage.Width, memoryImage.Height);
            using var memoryGraphics = Graphics.FromImage(memoryImage);
            memoryGraphics.CopyFromScreen(0, 0, 0, 0, size);
        }

        public static byte[] ImageToBytes(Image value)
        {
            var converter = new ImageConverter();
            var arr = (byte[])converter.ConvertTo(value, typeof(byte[]));
            return arr;
        }

        public static string EncodeBytes(byte[] value) => Convert.ToBase64String(value);

        public static byte[] DecodeBytes(string value) => Convert.FromBase64String(value);
    }
}