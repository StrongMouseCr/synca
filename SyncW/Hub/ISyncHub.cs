﻿using System.Drawing;
using System.Threading.Tasks;

namespace SyncW.Hub
{
    public interface ISyncHub
    {
        Task ReceiveImage (byte[] image);
    }
}