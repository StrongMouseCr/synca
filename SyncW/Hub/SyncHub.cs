﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace SyncW.Hub
{
    public class SyncHub : Hub<ISyncHub>
    {
        public override Task OnConnectedAsync()
        {
            return Task.CompletedTask;
        }
        
        public override Task OnDisconnectedAsync(Exception? exception)
        {
            return Task.CompletedTask;
        }
    }
}