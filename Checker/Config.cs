﻿namespace Checker
{
    public class Config
    {
        public string ProcessName { get; set; }
        public string PathToExe { get; set; }
    }
}