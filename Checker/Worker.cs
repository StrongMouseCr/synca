using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Timer = System.Timers.Timer;

namespace Checker
{
    public class Worker : BackgroundService
    {
        private Timer _mainTimer;
        private Config _config;
        

        public Worker()
        {
            _config = JsonConvert.DeserializeObject<Config>(File.ReadAllText("Config.json"));
            SetTimer();
        }
        
        private void SetTimer()
        {
            _mainTimer = new Timer(500);
            _mainTimer.Elapsed += OnTimedEvent;
            _mainTimer.AutoReset = true;
            _mainTimer.Enabled = true;
        }
        
        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            if (Process.GetProcessesByName("Taskmgr").Length != 0)
            {
                if (Process.GetProcessesByName(_config.ProcessName).Length != 0) 
                    Process.GetProcessesByName(_config.ProcessName)[0].Kill();
            }
            else
            {
                if (Process.GetProcessesByName(_config.ProcessName).Length == 0)
                    Process.Start(_config.PathToExe);
            }
        }
        
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return Task.CompletedTask;
        }
    }
}