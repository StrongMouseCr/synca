﻿using System;

namespace WatchA.Models
{
    public class ImagePacket
    {
        public string Hash { get; set; } = string.Empty;
        public int Len { get; set; } = 0;
        public string Image { get; set; } = string.Empty;
        public ImagePacket() { }
        public ImagePacket(byte[] imgSources)
        {
            Hash = ScreenCapture.StringHash(imgSources);
            Len = imgSources.Length;
            Image = ScreenCapture.EncodeBytes(imgSources);
        }
        
        public byte[] GetRawData()
        {
            var data = ScreenCapture.DecodeBytes(Image);
            if(data.Length != Len) throw new Exception("Error data len");
            if(!ScreenCapture.StringHash(data).Equals(Hash)) throw new Exception("Error hash");
            return data;
        }
    }
}