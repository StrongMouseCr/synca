﻿using System;
using System.Drawing;
using System.IO;
using System.Text;

namespace WatchA.Models
{
    public class ScreenCapture
    {
        public static Image TakeScreen()
        {
            var bitmap = new Bitmap(1920, 1080);
            var graphics = Graphics.FromImage(bitmap);
            graphics.CopyFromScreen(0, 0, 0, 0, bitmap.Size);
            return bitmap;
        }

        public static byte[] ImageToBytes(Image value)
        {
            var converter = new ImageConverter();
            var arr = (byte[])converter.ConvertTo(value, typeof(byte[]));
            return arr;
        }

        public static Image BytesToImage(byte[] value)
        {
            using(var ms = new MemoryStream(value))
            {
                return Image.FromStream(ms);
            }
        }

        public static string EncodeBytes(byte[] value) => Convert.ToBase64String(value);

        public static byte[] DecodeBytes(string value) => Convert.FromBase64String(value);

        public static string StringHash(byte[] value)
        {
            using (var md5 = System.Security.Cryptography.MD5.Create())
            {
                var hashBytes = md5.ComputeHash(value);
                var sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString().ToLower();
            }
        }
    }
}