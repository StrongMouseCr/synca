﻿namespace WatchA.Models
{
    public class Config
    {
        public string Protocol { get; set; }
        public string Ip { get; set; }
        public int Port { get; set; }
    }
}