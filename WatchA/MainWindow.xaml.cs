﻿using System;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Media.Imaging;
using Newtonsoft.Json;
using WatchA.Models;

namespace WatchA
{
    public partial class MainWindow : Window
    {
        private Config _config;
        private LogicSync _logic;

        public MainWindow()
        {
            InitializeComponent();
            _config = JsonConvert.DeserializeObject<Config>(File.ReadAllText("Config.json"));
            _logic = new LogicSync(_config.Protocol, _config.Ip, _config.Port);
            _logic.ReceiveImageFromServer += LogicSyncOnReceiveImageFromServer;
            _logic.ConnectionLost += LogicSyncOnConnectionLost;
            _logic.Connect();
        }

        private void LogicSyncOnConnectionLost()
        {
            _logic.ReceiveImageFromServer -= LogicSyncOnReceiveImageFromServer;
            Dispatcher.Invoke(() =>
                ImageHandler.Source =
                    new BitmapImage(new Uri(Path.Combine(Directory.GetCurrentDirectory(), "Homework.jpg"))));
        }

        private void LogicSyncOnReceiveImageFromServer(byte[] image)
        {
            ImageHandler.Source = ImageFromBuffer(image);
        }
        
        public BitmapImage ImageFromBuffer(byte[] bytes)
        {
            var stream = new MemoryStream(bytes);
            var image = new BitmapImage();
            image.BeginInit();
            image.StreamSource = stream;
            image.EndInit();
            return image;
        }
    }
}