﻿using System;
using System.Drawing;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using WatchA.Models;

namespace WatchA
{
    public class LogicSync
    {
        private HubConnection _hubConnection;
        
        public delegate void SyncHandler(byte[] image);
        public delegate void SyncErrorHandler();
        public event SyncHandler ReceiveImageFromServer;
        public event SyncErrorHandler ConnectionLost;

        public LogicSync(string protocol, string ip, int port)
        {
            _hubConnection =
                new HubConnectionBuilder().WithUrl($"{protocol}://{ip}:{port}/syncHub").Build();
            _hubConnection.Closed += HubConnectionOnClosed;
        }

        private Task HubConnectionOnClosed(Exception arg)
        {
            ConnectionLost?.Invoke();
            return Task.CompletedTask;
        }

        public async void Connect()
        {
            try
            {
                _hubConnection.On<byte[]>("ReceiveImage", ImageReceived);
                await _hubConnection.StartAsync();
            }
            catch
            {
                ConnectionLost?.Invoke();
            }
        }

        private void ImageReceived(byte[] image)
        {
            ReceiveImageFromServer?.Invoke(image);
        }
    }
}